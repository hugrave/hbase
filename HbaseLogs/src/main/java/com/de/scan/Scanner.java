package com.de.scan;

import com.de.logs.Log;
import com.de.logs.RowKey;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by edoardo on 14/12/2018.
 */
public class Scanner {

    public static List<Log> scan (Table table,
                                  RowKey startRK, RowKey endRK) throws IOException{
        List<byte[]> colFamilies = new ArrayList<>();

        // Extracting all the column families in the table
        HColumnDescriptor[] colFamiliesDesc = table.getTableDescriptor().getColumnFamilies();
        for (HColumnDescriptor cf : colFamiliesDesc) {
            colFamilies.add(cf.getName());
        }

        return scan (table, colFamilies, startRK, endRK);
    }

    public static List<Log> scan (Table table, List<byte[]> CF,
                                  RowKey startRK, RowKey endRK) throws IOException {

        List<Log> logs = new ArrayList<>();
        Scan s = new Scan(startRK.getRK(), endRK.getRK());
        for (byte[] family : CF) {
            s.addFamily(family);
        }

        // Querying the database
        ResultScanner rs = table.getScanner(s);

        // Building the list of logs
        for (Result r : rs) {
            RowKey rk = new RowKey(r.getRow());
            Log log = new Log(rk);
            Cell[] cells = r.rawCells();
            for (Cell c : cells) {
                log.addData(
                        CellUtil.cloneFamily(c),
                        CellUtil.cloneQualifier(c),
                        CellUtil.cloneValue(c)
                );
            }
            logs.add(log);
        }

        return logs;
    }

    public static void print (List<Log> logs) {
        for (Log log : logs) {
            new RowKey(log.getRowKey()).print();
            for (byte[][] triplet : log.getValues()) {
                System.out.println(
                    "\t" +
                    new String(triplet[0]) + "-> " +
                    new String(triplet[1]) + ": " +
                    new String(triplet[2])
                );
            }
            System.out.println();
        }
    }
}
