package com.de.logs;

import com.de.utils.constant.Constants;

import java.nio.ByteBuffer;

public class SecondaryRowIndex {
    private String errorType;

    public SecondaryRowIndex(String errorType){
        this.errorType = errorType;
    }

    public SecondaryRowIndex (byte[] rowKey) {
        decode(rowKey);
    }

    public static class Builder {

        private String errorType;

        public SecondaryRowIndex.Builder setErrorType(String errorType) {
            this.errorType = errorType;
            return this;
        }

        public SecondaryRowIndex build() {
            return new SecondaryRowIndex(this.errorType);
        }

    }


    public byte[] getRK() {
        byte[] rowKey = new byte[Constants.ET_SIZE];

        // Filling the small strings
        byte[] errorTypeBytes = filling(this.errorType, Constants.ET_SIZE);

        System.arraycopy(errorTypeBytes, 0, rowKey,
                0,
                Constants.ET_SIZE);

        return rowKey;
    }

    private byte[] filling(String string, int size) {
        byte[] bytes;
        string = (string == null) ? "" : string;

        int strLength = string.getBytes().length;
        if (strLength < size) {
            bytes = ByteBuffer.allocate(size).put(string.getBytes(), 0, strLength).array();
            for (int i=strLength; i < size; i++) {
                bytes[i] = Constants.FILLING_CHAR;
            }
        }
        else {
            bytes = string.getBytes();
        }
        return bytes;
    }

    private void decode(byte[] rowKey) {
        int rowKeyLength = Constants.ET_SIZE;
        ByteBuffer bf = ByteBuffer.allocate(rowKeyLength).put(rowKey);
        byte[] errorTypeBytes = new byte[Constants.ET_SIZE];

        bf.position(0);
        bf.get(errorTypeBytes, 0, Constants.ET_SIZE);

        this.errorType = new String(errorTypeBytes);
    }

    public void print(){
        System.out.println(this.errorType);
    }
}
