package com.de.logs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ErrorType {
    private SecondaryRowIndex rowKey;
    public Map<byte[], Map<byte[], byte[]>> data; // cf -> cq -> value

    public ErrorType (SecondaryRowIndex rowKey) {
        this.rowKey = rowKey;
        this.data = new HashMap<>();
    }

    public ErrorType addData (byte[] colFamily, byte[] colQualifier, byte[] value) {
        if ( !this.data.containsKey(colFamily) ) {
            this.data.put(colFamily, new HashMap<>());
        }
        Map<byte[], byte[]> cfData = this.data.get(colFamily);
        cfData.put(colQualifier, value);

        return this;
    }

    public byte[] getRowKey() {
        return this.rowKey.getRK();
    }

    public List<byte[][]> getValues() {
        List<byte[][]> list = new ArrayList<>();
        for (Map.Entry<byte[], Map<byte[], byte[]>> e1 : this.data.entrySet()) {
            for (Map.Entry<byte[], byte[]> e2 : e1.getValue().entrySet()) {
                byte[][] coord = new byte[3][];
                coord[0] = e1.getKey(); // CF
                coord[1] = e2.getKey(); // CQ
                coord[2] = e2.getValue(); // Value
                list.add(coord);
            }
        }
        return list;
    }
}
