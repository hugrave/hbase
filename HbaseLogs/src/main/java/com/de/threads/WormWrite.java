package com.de.threads;


import com.de.db.DB;
import com.de.logs.ErrorType;
import com.de.logs.Log;
import com.de.logs.RowKey;
import com.de.logs.SecondaryRowIndex;
import com.de.utils.Generator;
import com.de.utils.constant.Constants;
import com.de.utils.constant.WormConst;

import java.io.IOException;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by edoardo on 04/12/2018.
 */
public class WormWrite extends Thread {
    private int numWrites;
    private DB db;

    public WormWrite(int numWrites, DB db) {
        this.numWrites = numWrites;
        this.db = db;
    }

    @Override
    public void run() {
        Random rand = new Random();
        for (int i=0; i<this.numWrites; i++) {
            RowKey rowKey = new RowKey.Builder()
                    .setProductFamily(Constants.WORM_PF)
                    .setCompanyId(Generator.getRandomCID(rand))
                    .setLogType(Generator.getRandomLT(rand))
                    .setTimestamp(Long.toString(System.currentTimeMillis()))
                    .build();

            Log log = new Log(rowKey);

            // Adding data to the row
            // Message
            log.addData(
                    Constants.CF_HUMAN_READABLE,
                    Constants.MESSAGE,
                    WormConst.MESSAGE_LIST[rand.nextInt(WormConst.MESSAGE_LIST.length)]
            );

            // Execution data
            int numData = rand.nextInt(WormConst.DATA_CQ.length);
            for (int j=0; j < numData; j++) {
                int nextInt = rand.nextInt(WormConst.DATA_CQ.length);

                while (log.data.containsKey(WormConst.DATA_CQ[nextInt])) {
                    nextInt = rand.nextInt(WormConst.DATA_CQ.length);
                }

                byte[] cq = WormConst.DATA_CQ[nextInt];
                byte[] value = WormConst.DATA_MAP.get(cq)[rand.nextInt(WormConst.DATA_MAP.get(cq).length)];

                if (Arrays.equals(cq, WormConst.ERROR_TYPE)){
                    ErrorType errorType = new ErrorType(new SecondaryRowIndex(value));
                    errorType.addData(Constants.CF_LOGKEY, log.getRowKey(), log.getRowKey());
                    try {
                        db.putErrorType(errorType);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return;
                    }

                }

                log.addData(Constants.CF_DATA, cq, value);
            }

            // Loading the log to the DB

            try {

                db.putLog(log);
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }

        }
    }
}
