package com.de.utils.constant;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by edoardo on 04/12/2018.
 */
public class Constants {
    /*
    SIZES of rowKey components
     */
    public final static int PF_SIZE=4;
    public final static int CID_SIZE=4;
    public final static int LT_SIZE=3;
    public final static int TS_SIZE=13;
    public final static int ET_SIZE=25;

    public static final char FILLING_CHAR = ' ';

    /*
    Column families
     */
    public static final byte[] CF_HUMAN_READABLE = "h".getBytes();
    public static final byte[] CF_DATA = "d".getBytes();
    public static final byte[] CF_LOGKEY = "logs_index".getBytes();

    /*
    Product families for the application
     */
    public final static String ROBOT_PF = "ROBO";
    public final static String WORM_PF = "WORM";
    public final static String TROLL_PF = "TROLL";
    /*
    CompanyIDs
     */
    public static final String[] COMPANY_IDS = {"PROX", "ORNG", "BMW", "MERC"};
    /*
    Log Types
     */
    public static final String[] LOG_TYPES = {"ERR", "WAR", "INF"};

    /*
    Common data
     */
    public static final byte[] MESSAGE = "MESSAGE".getBytes();


}
