package com.de.utils.constant;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by edoardo on 09/12/2018.
 */
public class TrollConst {

    public static final byte[] JOB_ID = "Job ID".getBytes();
    public static final byte[] EXCEPTION_STACK = "Exception Stack".getBytes();
    public static final byte[] MAIN_PATH = "Main Path".getBytes();
    public static final byte[] JAVA_VERSION = "Java Version".getBytes();
    public static final byte[] ERROR_TYPE = "Error type".getBytes();

    private static final byte[][] JOB_ID_LIST = {
            "0".getBytes(),
            "1".getBytes(),
            "2".getBytes(),
            "3".getBytes(),
            "4".getBytes(),
            "5".getBytes(),
            "6".getBytes(),
            "7".getBytes(),
            "8".getBytes(),
            "9".getBytes(),
            "10".getBytes()
    };

    public static final byte[][] ERROR_TYPE_VALUE = {
            "NullPointerException".getBytes(),
            "NetworkException".getBytes(),
            "StaticMappingError".getBytes(),
            "TrollMalwareException".getBytes()
    };

    public static final byte[][] MESSAGE_LIST = {
            "Malware detected in the network".getBytes(),
            "The Malware could not be deleted".getBytes(),
            "Pure evil this troll I hate it".getBytes()
    };

    private static final byte[][] EXC_STACK_LIST = {
            ("MalwareException: Error occasioned by \n" +
                    "        at MalwareManager.checkVirusData(MalwareManager.java:130)\n" +
                    "        at MalwareProgram.main(RobotManager.java:9)").getBytes(),
            ("TroyanException: Nothing to do agains Troya\n" +
                    "        at TroyaManager.testBody(TroyaManager.java:165)\n" +
                    "        at TroyaProgram.main(TroyaManager.java:9)").getBytes(),
            ("VirusException: Completely infected the databaseg\n" +
                    "        at VirusManager.testBody(VirusManager.java:165)\n" +
                    "        at VirusProgram.main(VirusManager.java:9)").getBytes()
    };

    private static final byte[][] MAIN_PATH_LIST = {
            "/home/user/".getBytes(),
            "/Users/user/home".getBytes()
    };
    private static final byte[][] JV_LIST = {
            "1.8".getBytes(),
            "1.7".getBytes(),
            "1.6".getBytes(),
            "1.5".getBytes(),
            "1.4".getBytes()
    };

    public static final Map<byte[], byte[][]> DATA_MAP;
    static {
        DATA_MAP = new HashMap<>();
        DATA_MAP.put(JOB_ID, JOB_ID_LIST);
        DATA_MAP.put(EXCEPTION_STACK, EXC_STACK_LIST);
        DATA_MAP.put(MAIN_PATH, MAIN_PATH_LIST);
        DATA_MAP.put(JAVA_VERSION, JV_LIST);
        DATA_MAP.put(ERROR_TYPE, ERROR_TYPE_VALUE);
    }
    public final static byte[][]  DATA_CQ = {JOB_ID, EXCEPTION_STACK, MAIN_PATH, JAVA_VERSION, ERROR_TYPE};
}
