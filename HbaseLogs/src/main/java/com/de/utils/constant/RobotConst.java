package com.de.utils.constant;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by edoardo on 09/12/2018.
 */
public class RobotConst {

    public static final byte[] JOB_ID = "Job ID".getBytes();
    public static final byte[] EXCEPTION_STACK = "Exception Stack".getBytes();
    public static final byte[] MAIN_PATH = "Main Path".getBytes();
    public static final byte[] JAVA_VERSION = "Java Version".getBytes();
    public static final byte[] ERROR_TYPE = "Error type".getBytes();

    private static final byte[][] JOB_ID_LIST = {
            "0".getBytes(),
            "1".getBytes(),
            "2".getBytes(),
            "3".getBytes(),
            "4".getBytes(),
            "5".getBytes(),
            "6".getBytes(),
            "7".getBytes(),
            "8".getBytes(),
            "9".getBytes(),
            "10".getBytes()
    };

    public static final byte[][] ERROR_TYPE_VALUE = {
            "NullPointerException".getBytes(),
            "NetworkException".getBytes(),
            "StaticMappingError".getBytes()
    };

    public static final byte[][] MESSAGE_LIST = {
            "Error finding geolocation data".getBytes(),
            "Left arm is not responding".getBytes(),
            "Right arm is not responding".getBytes()
    };

    private static final byte[][] EXC_STACK_LIST = {
            ("MissingDataException: Error finding geolocation data\n" +
                    "        at RobotManager.checkGeoData(RobotManager.java:130)\n" +
                    "        at RobotProgram.main(RobotManager.java:9)").getBytes(),
            ("MechanicalException: Left arm is not responding\n" +
                    "        at RobotManager.testBody(RobotManager.java:165)\n" +
                    "        at RobotProgram.main(RobotManager.java:9)").getBytes(),
            ("MechanicalException: Right arm is not responding\n" +
                    "        at RobotManager.testBody(RobotManager.java:165)\n" +
                    "        at RobotProgram.main(RobotManager.java:9)").getBytes()
    };

    private static final byte[][] MAIN_PATH_LIST = {
            "/home/user/".getBytes(),
            "/Users/user/home".getBytes()
    };
    private static final byte[][] JV_LIST = {
            "1.8".getBytes(),
            "1.7".getBytes(),
            "1.6".getBytes(),
            "1.5".getBytes(),
            "1.4".getBytes()
    };

    public static final Map<byte[], byte[][]> DATA_MAP;
    static {
        DATA_MAP = new HashMap<>();
        DATA_MAP.put(JOB_ID, JOB_ID_LIST);
        DATA_MAP.put(EXCEPTION_STACK, EXC_STACK_LIST);
        DATA_MAP.put(MAIN_PATH, MAIN_PATH_LIST);
        DATA_MAP.put(JAVA_VERSION, JV_LIST);
        DATA_MAP.put(ERROR_TYPE, ERROR_TYPE_VALUE);
    }
    public final static byte[][]  DATA_CQ = {JOB_ID, EXCEPTION_STACK, MAIN_PATH, JAVA_VERSION, ERROR_TYPE};
}
