package com.de.utils;

import com.de.utils.constant.Constants;

import java.util.*;

/**
 * Created by edoardo on 06/12/2018.
 */
public class Generator {
    public static String getRandomCID(Random rand) {
        //List<String> keyList = new ArrayList<>(Constants.COMPANY_ID_MAP.keySet());
        //return Constants.COMPANY_ID_MAP.get(keyList.get(rand.nextInt(keyList.size())));
        return Constants.COMPANY_IDS[rand.nextInt(Constants.COMPANY_IDS.length)];
    }

    public static String getRandomLT(Random rand) {
        return Constants.LOG_TYPES[rand.nextInt(Constants.LOG_TYPES.length)];
    }
}
