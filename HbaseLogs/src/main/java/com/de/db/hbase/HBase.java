package com.de.db.hbase;

import com.de.db.DB;
import com.de.logs.ErrorType;
import com.de.logs.Log;
import com.de.logs.RowKey;
import com.de.utils.constant.Constants;
import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.client.*;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.*;

/**
 * Created by edoardo on 09/12/2018.
 */
public class HBase implements DB {

    private Connection conn;
    private Table table;
    private Table secTable;

    public void getConnection(String tableNameStr) throws IOException{
        Configuration config;
        try {
            config = checkConnection();
        } catch (ServiceException e) {
            throw new IOException(e.getMessage());
        }
        TableName tableName = TableName.valueOf(tableNameStr);
        TableName secTableName = TableName.valueOf("ErrorType");
        // Opening the connection
        this.conn = ConnectionFactory.createConnection(config);

        this.secTable = conn.getTable(secTableName);
        this.table = conn.getTable(tableName);

    }

    @Override
    public void configure(String tableNameStr) throws IOException {
        Configuration config;
        try {
            config = checkConnection();
        } catch (ServiceException e) {
            throw new IOException(e.getMessage());
        }
        TableName tableName = TableName.valueOf(tableNameStr);
        byte[] hr_cf = Constants.CF_HUMAN_READABLE;
        byte[] d_cf = Constants.CF_DATA;

        TableName secTableName = TableName.valueOf("ErrorType");
        byte[] cf = Constants.CF_LOGKEY;

        // Opening the connection
        this.conn = ConnectionFactory.createConnection(config);
        // Creating the admin instance
        Admin admin = conn.getAdmin();

        // Creating the table
        HTableDescriptor desc = new HTableDescriptor(tableName);
        desc.addFamily(new HColumnDescriptor(hr_cf));
        desc.addFamily(new HColumnDescriptor(d_cf));
        try {
            admin.createTable(desc);
        }
        catch (TableExistsException e) {
            if (admin.isTableEnabled(tableName))
                admin.disableTable(tableName);
            admin.deleteTable(tableName);
            admin.createTable(desc);
        }

        desc = new HTableDescriptor(secTableName);
        desc.addFamily(new HColumnDescriptor(cf));

        try {
            admin.createTable(desc);
        }
        catch (TableExistsException e) {
            if (admin.isTableEnabled(secTableName))
                admin.disableTable(secTableName);
            admin.deleteTable(secTableName);
            admin.createTable(desc);
        }

        this.secTable = conn.getTable(secTableName);
        this.table = conn.getTable(tableName);
    }

    private Configuration checkConnection() throws IOException, ServiceException {
        Configuration config = HBaseConfiguration.create();

        HBaseAdmin.checkHBaseAvailable(config);
        System.out.println("Connection successful!");
        return config;
    }


    @Override
    public void putLog(Log log) throws IOException {
        Put put = new Put(log.getRowKey());
        List<byte[][]> listCoord = log.getValues();
        for (byte[][] coord : listCoord) {
            put.addColumn(coord[0], coord[1], coord[2]);
        }
        this.table.put(put);
    }

    @Override
    public void putErrorType(ErrorType log) throws IOException {
        Put put = new Put(log.getRowKey());
        List<byte[][]> listCoord = log.getValues();
        for (byte[][] coord : listCoord) {
            put.addColumn(coord[0], coord[1], coord[2]);
        }
        this.secTable.put(put);
    }

    public Connection getConn() {
        return conn;
    }

    public Table getTable() {
        return table;
    }

    public Table getSecTable() {
        return secTable;
    }

    public void printTable() throws IOException {
        Scan s = new Scan();
        ResultScanner rs = this.table.getScanner(s);
        for (Result r : rs) {
            new RowKey(r.getRow()).print();
            //System.out.println(new String(r.getRow()));
            Cell[] cells = r.rawCells();
            for (Cell c : cells) {
                String cf = new String(CellUtil.cloneFamily(c));
                String cq = new String(CellUtil.cloneQualifier(c));
                String value = new String(CellUtil.cloneValue(c));
                System.out.println("\t" +
                    cf + ", " +
                    cq + ": " +
                    value
                );
            }
        }



    }

}
