package com.de;

import com.de.db.hbase.HBase;
import com.de.logs.Log;
import com.de.logs.RowKey;
import com.de.scan.Scanner;
import com.de.threads.RobotWrite;
import com.de.threads.TrollWrite;
import com.de.threads.WormWrite;

import java.io.IOException;
import java.util.List;

/**
 * Created by edoardo on 02/12/2018.
 */
public class Main {

    public static final String tableNameStr = "Logs";
    public static final int nRows = 100;

    public static void main (String args[]) {
        HBase hbase = new HBase();

        try {
            //hbase.getConnection(tableNameStr);
            hbase.configure(tableNameStr);
            RobotWrite rw = new RobotWrite(nRows, hbase);
            WormWrite ww = new WormWrite(nRows, hbase);
            TrollWrite tw = new TrollWrite(nRows, hbase);
            rw.start();
            rw.join();
            ww.start();
            ww.join();
            tw.start();
            tw.join();
            hbase.printTable();

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            /*
            Reading all error logs
             */
            RowKey startRK = new RowKey.Builder()
                    .setLogType("ERR")
                    .build();
            RowKey endRK = new RowKey.Builder()
                    .setLogType("ERS")
                    .build();
            List<Log> logs = Scanner.scan(hbase.getTable(), startRK, endRK);
            //Scanner.print(logs);

            /*
            Get all the logs for ROBO product family
             */
            String[] logTypes = {"INF", "WAR", "ERR"};
            for (String logType : logTypes) {
                startRK = new RowKey.Builder()
                        .setLogType(logType)
                        .setProductFamily("ROBO")
                        .build();
                endRK = new RowKey.Builder()
                        .setLogType(logType)
                        .setProductFamily("ROBP")
                        .build();
                logs = Scanner.scan(hbase.getTable(), startRK, endRK);
                Scanner.print(logs);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
