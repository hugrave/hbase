package com.de.db;

import com.de.logs.Log;

import java.io.IOException;

/**
 * Created by edoardo on 09/12/2018.
 */
public interface DB {
    void configure(String tableNameStr) throws IOException;
    void putLog(Log log) throws IOException;
}
