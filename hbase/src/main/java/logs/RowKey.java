package com.de.logs;

import com.de.utils.constant.Constants;

import java.nio.ByteBuffer;

/**
 * Created by edoardo on 04/12/2018.
 */
public class RowKey {
    /*
    STRUCTURE OF THE ROW KEY
    ProductFamily   -> 4 bytes
    CompanyID       -> 4 bytes
    LogType         -> 3 bytes (INF, WAR, ERR)
    TimeStamp       -> 5 bytes
     */
    private String productFamily;
    private String companyId;
    private String logType;
    private String timestamp;

    public RowKey(String productFamily, String companyId, String logType, String timestamp) {
        this.productFamily = productFamily;
        this.companyId = companyId;
        this.logType = logType;
        this.timestamp = timestamp;
    }

    public RowKey (byte[] rowKey) {
        decode(rowKey);
    }


    // Builder design pattern
    public static class Builder {

        private String productFamily;
        private String companyId;
        private String logType;
        private String timestamp;

        public Builder setProductFamily(String productFamily) {
            this.productFamily = productFamily;
            return this;
        }
        public Builder setCompanyId(String companyId) {
            this.companyId = companyId;
            return this;
        }
        public Builder setLogType(String logType) {
            this.logType = logType;
            return this;
        }
        public Builder setTimestamp(String timestamp) {
            this.timestamp = timestamp;
            return this;
        }
        public RowKey build() {
            return new RowKey(
                    this.productFamily, this.companyId,
                    this.logType, this.timestamp
            );
        }

    }

    public void setProductFamily(String productFamily) {
        this.productFamily = productFamily;
    }
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
    public void setLogType(String logType) {
        this.logType = logType;
    }
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getProductFamily() {
        return productFamily;
    }
    public String getCompanyId() {
        return companyId;
    }
    public String getLogType() {
        return logType;
    }
    public String getTimestamp() {
        return timestamp;
    }


    public byte[] getRK() {
        byte[] rowKey = new byte[
                Constants.PF_SIZE + Constants.CID_SIZE +
                Constants.LT_SIZE + Constants.TS_SIZE
                ];

        // Filling the small strings
        byte[] productFamilyBytes = filling(this.productFamily, Constants.PF_SIZE);
        byte[] companyIDBytes = filling(this.companyId, Constants.CID_SIZE);
        byte[] logTypeBytes = filling(this.logType, Constants.LT_SIZE);
        byte[] timestampBytes = filling(this.timestamp, Constants.TS_SIZE);

        System.arraycopy(productFamilyBytes, 0, rowKey,
                0,
                Constants.PF_SIZE);

        System.arraycopy(companyIDBytes, 0, rowKey,
                Constants.PF_SIZE,
                Constants.CID_SIZE);

        System.arraycopy(logTypeBytes, 0, rowKey,
                Constants.CID_SIZE+Constants.PF_SIZE,
                Constants.LT_SIZE);

        System.arraycopy(timestampBytes, 0, rowKey,
                Constants.CID_SIZE+Constants.PF_SIZE+Constants.LT_SIZE,
                Constants.TS_SIZE);

        return rowKey;
    }

    private byte[] filling(String string, int size) {
        byte[] bytes;
        string = (string == null) ? "" : string;

        int strLength = string.getBytes().length;
        if (strLength < size) {
            bytes = ByteBuffer.allocate(size).put(string.getBytes(), 0, strLength).array();
            for (int i=strLength; i < size; i++) {
                bytes[i] = Constants.FILLING_CHAR;
            }
        }
        else {
            bytes = string.getBytes();
        }
        return bytes;
    }

    private void decode(byte[] rowKey) {
        int rowKeyLength = Constants.PF_SIZE +Constants.CID_SIZE + Constants.LT_SIZE + Constants.TS_SIZE;
        ByteBuffer bf = ByteBuffer.allocate(rowKeyLength).put(rowKey);
        byte[] productFamilyBytes = new byte[Constants.PF_SIZE];
        byte[] companyIDBytes = new byte[Constants.CID_SIZE];
        byte[] logTypeBytes = new byte[Constants.LT_SIZE];
        byte[] timestampBytes = new byte[Constants.TS_SIZE];

        bf.position(0);
        bf.get(productFamilyBytes, 0, Constants.PF_SIZE);
        bf.get(companyIDBytes, 0, Constants.CID_SIZE);
        bf.get(logTypeBytes, 0, Constants.LT_SIZE);
        bf.get(timestampBytes, 0, Constants.TS_SIZE);

        this.productFamily = new String(productFamilyBytes);
        this.companyId = new String(companyIDBytes);
        this.logType = new String(logTypeBytes);
        this.timestamp = new String(timestampBytes);
    }

    public void print() {
        System.out.println(
            this.productFamily + " + " +
            this.companyId + " + " +
            this.logType + " + " +
            this.timestamp
        );
    }
}
